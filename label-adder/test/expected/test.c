// This file is part of TestCov,
// a robust test executor with reliable coverage measurement:
// https://gitlab.com/sosy-lab/software/test-suite-validator/
//
// SPDX-FileCopyrightText: 2019 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

extern int __VERIFIER_nondet_int();

int main() {
Goal_1:;

  int x = __VERIFIER_nondet_int();
  if (x > 0) {
  Goal_3:;
  
    x++;
  } else {
  Goal_4:;
  
    x--;
  }
  Goal_2:;
  
}
