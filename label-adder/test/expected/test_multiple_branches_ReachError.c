// This file is part of TestCov,
// a robust test executor with reliable coverage measurement:
// https://gitlab.com/sosy-lab/software/test-suite-validator/
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

extern char __VERIFIER_nondet_char();

void reach_error() {
Goal_1:;
}

int main() {
Goal_2:;

  char a = __VERIFIER_nondet_char();
  char b = __VERIFIER_nondet_char();
  char c = __VERIFIER_nondet_char();
  if (c == 16) {
  Goal_4:;
  
    c = __VERIFIER_nondet_char();
    if (a == 'a' && b == 5) {
    Goal_5:;
    
        reach_error();
    }
  }

Goal_3:;

}